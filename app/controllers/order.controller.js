
// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Review Model
const  orderModel = require("../models/orderModels");

const userModels = require("../models/userModels");

const createOerder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.usersId;

    const body = request.body;
    // console.log(body)
    // { bodyStars: 5, bodyNote: 'Good' }

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "users ID không hợp lệ"
        })
    }
    if(!body.pizzaType) {
        return response.status(400).json({
            status: "Bad Request",
            message: "pizzaType không hợp lệ"
        })
    }
    if(isNaN(body.pizzaType) || body.pizzaType < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No pizzaType không hợp lệ"
        })
    }
    if(!body.status) {
        return response.status(400).json({
            status: "Bad Request",
            message: "status không hợp lệ"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    const newReview = {
        _id: mongoose.Types.ObjectId(),
        pizzaType: body.pizzaType,
        pizzaCode: body.pizzaCode,
        status: body.status,

    }
     console.log(newReview)
    orderModel.create(newReview, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        userModels.findByIdAndUpdate(userId, {
            $push: {
                orders: data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            return response.status(201).json({
                status: "Create Review Successfully",
                data: data
            })
        })
    
    })
}


const getAllOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all review successfully",
            data: data
        })
    })
};

const getAllOrderkOfUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const userId = request.params.usersId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Course ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    userModels.findById(userId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all order of course successfully",
                data: data
            })
        })
}

const getAllOrderById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderId = request.params.orderId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    orderModel.findById(OrderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail Drink successfully",
            data: data
        })
    })
}

const updateOrderByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
     const orderId = request.params.orderId
    // B2: Validate dữ liệu
   

    // B3: Gọi Model tạo dữ liệu
    const updateOrder = {}

    if (body.bodyStars !== undefined) {
        updateOrder.pizzaType = body.pizzaType
    }

    if (body.pizzaCode !== undefined) {
        updateOrder.pizzaCode = body.pizzaCode
    }

    if (body.status !== undefined) {
        updateOrder.status = body.status
    }
    reviewModel.findByIdAndUpdate(orderId, updateOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update reivew successfully",
            data: data
        })
    })
}

const deleteOrderByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
   
    const orderId = request.params.orderIdId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewID không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Delete course successfully"
        })
        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
       
    })
}

module.exports = {
    getAllOrder,
    createOerder,
    getAllOrderById,
    getAllOrderkOfUser,
    updateOrderByID,
    deleteOrderByID
}