
// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Voucher Model
const voucherkModel = require("../models/voucherModel");
// Import Module Order Model
const  orderModel = require("../models/orderModels");


const getAllVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    voucherkModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all Voucher successfully",
            data: data
        })
    })
}

const createVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
    //
    const orderId = request.params.orderId;

    //console.log(body)
   // B2: Validate dữ liệu
    //Kiểm tra maNuocuong có hợp lệ hay không
    if(!body.maVoucher) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }
     //Kiểm tra maNuocuong có hợp lệ hay không
    
     // Kiểm tra donGia có hợp lệ hay không
    if(isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No Student không hợp lệ"
        })
    }
    //B3: Gọi Model tạo dữ liệu
    const newVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    };
    console.log(newVoucher)
    voucherkModel.create(newVoucher, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        };
        orderModel.findByIdAndUpdate(orderId, {
            $push: {
                voucher: data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create Review Successfully",
                data: data
            })
        })
    })
 }

const getVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const Voucher = request.params.voucherId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Voucher)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    voucherkModel.findById(Voucher, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail Voucher successfully",
            data: data
        })
    })
};

const getAllVoucherOfOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Course ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(OrderId)
        .populate("voucher")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all order of course successfully",
                data: data
            })
        })
}

const updateVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const Voucher = request.params.voucherId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Voucher)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher không hợp lệ"
        })
    }

    if(body.maVoucher !== undefined && body.maVoucher.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }
    
    if(body.phanTramGiamGia !== undefined && ( isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0 )) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No Student không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateVoucher = {}

    if(body.maVoucher !== undefined) {
        updateVoucher.maVoucher = body.maVoucher
    }

    if(body.tenNuocUong !== undefined) {
        updateVoucher.tenNuocUong = body.tenNuocUong
    }

    if(body.phanTramGiamGia !== undefined) {
        updateVoucher.phanTramGiamGia = body.phanTramGiamGia
    }
     console.log(updateVoucher)
    voucherkModel.findByIdAndUpdate(Voucher, updateVoucher, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update Voucher successfully",
            data: data
        })
    })
}

const deleteVoucherByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const  Voucher = request.params.voucherId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(Voucher)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    voucherkModel.findByIdAndDelete(Voucher, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete Voucher successfully"
        })
    })
}

module.exports = {
    getAllVoucher,
    createVoucher,
    getVoucherById,
    getAllVoucherOfOrder,
    updateVoucher,
    deleteVoucherByID
}