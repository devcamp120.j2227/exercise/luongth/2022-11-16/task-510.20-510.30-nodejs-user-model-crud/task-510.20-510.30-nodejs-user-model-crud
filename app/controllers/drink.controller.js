// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Drink Model
const drinkModel = require("../models/drinkModel");
const  orderModel = require("../models/orderModels");
const getAllDrink = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    drinkModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all Drink successfully",
            data: data
        })
    })
}

const createDrink = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;
    const body = request.body;
    console.log(body)
    //B2: Validate dữ liệu
    //Kiểm tra maNuocuong có hợp lệ hay không
    if(!body.maNuocUong) {
        return response.status(400).json({
            status: "Bad Request",
            message: "maNuocUong không hợp lệ"
        })
    }
     //Kiểm tra maNuocuong có hợp lệ hay không
    if(!body.tenNuocUong) {
        return response.status(400).json({
            status: "Bad Request",
            message: "tenNuocUong không hợp lệ"
        })
    }
    // // Kiểm tra donGia có hợp lệ hay không
    if(isNaN(body.donGia) || body.donGia < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No donGia không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newDrink = {
        _id: mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    }

    drinkModel.create(newDrink, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        orderModel.findByIdAndUpdate(orderId, {
            $push: {
                drink: data._id
            }
        }, (err, updatedCourse) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create Review Successfully",
                data: data
            })
        })
    })
};
const getAllDrinkOfOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const OrderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(OrderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Course ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(OrderId)
        .populate("drink")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Get all order of course successfully",
                data: data
            })
        })
}

const getDrinkById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const DrinkId = request.params.drinkId;
    
    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(DrinkId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    drinkModel.findById(DrinkId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail Drink successfully",
            data: data
        })
    })
}

const updateDrinkId = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const DrinkId = request.params.drinkId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(DrinkId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }

    if(body.maNuocUong !== undefined && body.maNuocUong.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }
    if(body.tenNuocUong !== undefined && body.tenNuocUong.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Title không hợp lệ"
        })
    }
    if(body.donGia !== undefined && ( isNaN(body.donGia) || body.donGia < 0 )) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No Student không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateDrink = {}

    if(body.maNuocUong !== undefined) {
        updateDrink.maNuocUong = body.maNuocUong
    }

    if(body.tenNuocUong !== undefined) {
        updateDrink.tenNuocUong = body.tenNuocUong
    }

    if(body.donGia !== undefined) {
        updateDrink.donGia = body.donGia
    }

    drinkModel.findByIdAndUpdate(DrinkId, updateDrink, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update Drink successfully",
            data: data
        })
    })
}

const deleteDrinkByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const DrinkId = request.params.drinkId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(DrinkId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "DrinkID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    drinkModel.findByIdAndDelete(DrinkId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete Drink successfully"
        })
    })
}

module.exports = {
    getAllDrink,
    createDrink,
    getDrinkById,
    getAllDrinkOfOrder,
    updateDrinkId,
    deleteDrinkByID
}