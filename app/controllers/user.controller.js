
// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Review Model
const userModel = require("../models/userModels");
const orderModel = require("../models/orderModels");

const createUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
     console.log(body)
    // { bodyStars: 5, bodyNote: 'Good' }

    // B2: Validate dữ liệu
    if(!body.fullName) {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    };
    if(!body.fullName) {
        return response.status(400).json({
            status: "Bad Request",
            message: "fullName không hợp lệ"
        })
    }
    if(!body.email) {
        return response.status(400).json({
            status: "Bad Request",
            message: "email không hợp lệ"
        })
    }
    if(!body.phone) {
        return response.status(400).json({
            status: "Bad Request",
            message: "address không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    const newUser = {
        _id: mongoose.Types.ObjectId(),
        fullName:body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone, 
    }
    userModel.create(newUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
            return response.status(201).json({
                status: "Create User Successfully",
                data: data
            })
    })
}

const getUserById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const UserId = request.params.UserId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(UserId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    userModel.findById(UserId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail User successfully",
            data: data
        })
    })
}

const getAllUser = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all User successfully",
            data: data
        })
    })
}



const updateUserbyId = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const UserId = request.params.UserId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(UserId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "UserID không hợp lệ"
        })
    }

    if (body.bodyStars !== undefined && !(Number.isInteger(body.bodyStars) && body.bodyStars > 0 && body.bodyStars <= 5)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Rate không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateUser = {}

    if (body.bodyStars !== undefined) {
        updateUser.stars = body.bodyStars
    }

    if (body.bodyNote !== undefined) {
        updateUser.note = body.bodyNote
    }

    userModel.findByIdAndUpdate(UserId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update reivew successfully",
            data: data
        })
    })
}

const deleteUserByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
   // B1: Chuẩn bị dữ liệu
   const UserId = request.params.usersId;
   // B2: Validate dữ liệu
   if(!mongoose.Types.ObjectId.isValid(UserId)) {
       return response.status(400).json({
           status: "Bad Request",
           message: "DrinkID không hợp lệ"
       })
   }

   // B3: Gọi Model tạo dữ liệu
   drinkModel.findByIdAndDelete(UserId, (error, data) => {
       if(error) {
           return response.status(500).json({
               status: "Internal server error",
               message: error.message
           })
       }

       return response.status(200).json({
           status: "Delete Drink successfully"
       })
   })
}

module.exports = {
    getUserById,
    createUser,
    getAllUser,
    updateUserbyId,
    deleteUserByID
}