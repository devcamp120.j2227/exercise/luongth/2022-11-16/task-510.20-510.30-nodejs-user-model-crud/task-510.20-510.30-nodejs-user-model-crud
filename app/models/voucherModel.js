//Import thư viện mongoose
const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance reviewSchema từ Class Schema
const voucherSchema = new Schema({
    // _id: {
    //     type: Number,
    //     default: 0
    // },
    maVoucher: {
        type: String,
        unipue:true,
        required: true,
    },
    phanTramGiamGia:{
        type:Number,
        repuired:true
    },
    ghiChu:{
        type:String,
        required:false
    }
}, {
    // Ghi dấu bản ghi được tạo hay cập nhật vào thời gian nào
    timestamps: true
});

// Biên dịch Review Model từ reviewSchema
module.exports = mongoose.model("Voucher", voucherSchema);