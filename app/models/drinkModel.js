// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const drinkSchema = new Schema({
    maNuocUong: {
        type: String,
        required: true,
        unique: true
    },
    tenNuocUong: {
        type:String,
        required: true,
    },
     donGia: {
        type: Number,
        required:true
    },
    
}, {
    timestamps: true
});


module.exports = mongoose.model("Drink", drinkSchema);
 