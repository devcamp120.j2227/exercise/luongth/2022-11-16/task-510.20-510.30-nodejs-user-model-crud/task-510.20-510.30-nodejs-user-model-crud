// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const userSchema = new Schema({
   
    fullName: {
        type: String,
        required: true,
        
    },
    email: {
        type:String,
        required: true,
        unique: true
    },
     address: {
        type: String,
        required: true
    },
    phone: {
        type: Number,
        required: true,
        unique:true
    },
    orders:[{
        type: mongoose.Types.ObjectId,
            ref: "Order"
    }]
    // // Một course có nhiều review
    // reviews: [{
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }]
    // Trường hợp 1 course có 1 reivew 
    // reviews: {
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }
}, {
    timestamps: true
})

module.exports = mongoose.model("User", userSchema);