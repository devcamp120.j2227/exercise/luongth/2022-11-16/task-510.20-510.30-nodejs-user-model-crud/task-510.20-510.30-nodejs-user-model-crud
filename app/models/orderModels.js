// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");
const randtoken = require('rand-token');
// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const OrderSchema = new Schema({
   
    orderCode: {
        type: String,
        default: function() {
            return randtoken.generate(5);
        }
    },
   
    pizzaCode: {
        type:String,
        required: true,
        unique: true
    },
    pizzaType: {
        type: Number,
        required: true
    },
    
    voucher:[{
        type: mongoose.Types.ObjectId,
            ref: "Voucher"
    }],
    drink:[{
        type: mongoose.Types.ObjectId,
            ref: "Drink"
    }],
    status:{
        type:String,
           required:true
    },
    
    // // Một course có nhiều review
    // reviews: [{
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }]
    // Trường hợp 1 course có 1 reivew 
    // reviews: {
    //     type: mongoose.Types.ObjectId,
    //     ref: "Review"
    // }
}, {
    timestamps: true
})

module.exports = mongoose.model("Order", OrderSchema);