// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
const VoucherController = require("../controllers/voucher.conitrollers")

router.get("/vouchers",  VoucherController.getAllVoucher);

router.post("/orders/:orderId/vouchers",  VoucherController.createVoucher);

router.get("/orders/:orderId/vouchers",VoucherController.createVoucher);

router.get("/vouchers/:voucherId", VoucherController.getVoucherById)

router.put("/vouchers/:voucherId",  VoucherController.updateVoucher)

router.delete("/vouchers/:voucherId", VoucherController.deleteVoucherByID)

module.exports = router;
