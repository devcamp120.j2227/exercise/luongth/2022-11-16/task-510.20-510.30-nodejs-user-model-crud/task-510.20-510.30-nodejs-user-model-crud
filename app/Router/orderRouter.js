
// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const drinkMiddleware = require("../middleware/drinkmiddleware");

const orderController = require("../controllers/order.controller");

router.get("/orders",orderController.getAllOrder);

router.post("/users/:usersId/orders", orderController.createOerder);

router.get("/users/:usersId/orders", orderController.getAllOrderkOfUser);

router.get("/orders/:orderId", orderController.getAllOrderById)

router.put("/orders/:orderId",  orderController.updateOrderByID)

router.delete("/orders/:orderId", orderController.deleteOrderByID)
module.exports = router