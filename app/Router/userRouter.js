

// Khai báo thư viện ExpressJS
const path = require("path");
// Khai báo router app
// Import course middleware
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const courseMiddleware = require("../middlewares/courseMiddleware");

// Import course controller
const userController = require("../controllers/user.controller");

router.get("/users",userController. getUserById);
router.post("/users", userController.createUser);

router.get("/users/:usersId", userController.getUserById)

router.put("/users/:usersId",  userController.updateUserbyId)

router.delete("/users/:usersId", userController.deleteUserByID)

module.exports = router;