


// Khai báo thư viện ExpressJS
// const express = require("express");

// // Khai báo router app
// const router = express.Router();

// // Import course middleware
// //const courseMiddleware = require("../Router/");

// router.get("/drinks", (request, response) => {
//     response.json({
//         message: "Get ALL Drinks"
//     })
// })
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
//const courseMiddleware = require("../middlewares/courseMiddleware");

// Import course controller
const DrinkController = require("../controllers/drink.controller")

router.get("/drinks",  DrinkController.getAllDrink);

router.post("/orders/:orderId/drinks", DrinkController.createDrink);

router.get("/orders/:orderId/drinks",DrinkController.getAllDrinkOfOrder);

router.get("/drinks/:drinkId", DrinkController.getDrinkById)

router.put("/drinks/:drinkId",  DrinkController.updateDrinkId)

router.delete("/drinks/:drinkId", DrinkController.deleteDrinkByID)

module.exports = router;