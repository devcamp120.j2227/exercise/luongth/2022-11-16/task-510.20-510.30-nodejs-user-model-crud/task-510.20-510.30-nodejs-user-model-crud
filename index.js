const { request, response } = require("express");
const express = require("express"); // Tương tự : import express from "express";

const path = require("path");


var mongoose = require('mongoose');

mongoose.connect("mongodb://127.0.0.1:27017/pizza365", function(error) {
    if (error) throw error;
    console.log('Successfully connected');
   })
   
// const { userClassList } = require("./courseRouter"); // Tương tự: import {data} from "./data";

// Khởi tạo Express App
const app = express();

const port = 8000;

// Cấu hình để API đọc được body JSON
app.use(express.json());

// Cấu hình để API đọc được body có ký tự tiếng Việt
app.use(express.urlencoded({
    extended: true
}))

// Khai báo Shema app
// /s
// Khai báo router app
const drinkRouter = require("./app/Router/drinkRouter");
const voucherRouter = require("./app/Router/voucherRouter");
const userRouter = require("./app/Router/userRouter");
const orderRouter = require("./app/Router/orderRouter");



app.use((request, response, next) => {
    console.log("Current time: ", new Date());
    next();
})

//  app.get("/", (request, response) => {
//     console.log(__dirname);
//      //response.send( "<h1>Hello world<h1>")
//     response.sendFile(path.join(__dirname + "/views/pizza365index.html"))
// });



app.use(express.static(__dirname + "./app/views"))
// App sử dụng router
app.use("/api", drinkRouter);
app.use("/api", voucherRouter);
app.use("/api", userRouter);
app.use("/api", orderRouter);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
